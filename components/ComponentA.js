import React from 'react';
import { View, Text, Button } from 'react-native';
import {useSelector, useDispatch } from 'react-redux';
import { increment, decrement } from '../redux/action.js';

function ComponentA() {

	const counter = useSelector(state => state);
	const dispatch = useDispatch();

	return (
		<View>
			<Text>ComponentA {counter.number}</Text>
			<Button title="+" onPress={() => dispatch(increment())}/>
			<Button title="-" onPress={() => dispatch(decrement())}/>
		</View>
	)
}

export default ComponentA;
