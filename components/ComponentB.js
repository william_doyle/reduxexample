import React from 'react';
import { View, Text} from 'react-native';
import {useSelector} from 'react-redux';

function ComponentB () {
	const counter = useSelector(state => state);

	return (
		<View>
			<Text> ComponentB {counter.number} </Text>
		</View>
	)
}

export default ComponentB;
