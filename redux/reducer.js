const initialState = {
	number: 0,
	words: [],
	currentWord: '',
}

export function counterReducer (state = initialState, action) {
	switch (action.type) {
		case 'INCREMENT':
			return Object.assign({}, state, {
				number: state.number +1
			})
		case 'DECREMENT':
			return Object.assign({}, state, {
				number: state.number -1
			})
		case 'ADDWORD':
			return{
				...state,
				words: [...state.words, action.wordToAdd]
			}
		case 'SETCURRENTWORD':
			return{
				...state,
				currentWord: action.wordToSet
			}
		default:
			return state;
	}
}
