export function increment(){
	return {
		type: 'INCREMENT',
	}
}


export function decrement() {
	return {
		type:'DECREMENT',
	}
}

export function addWord(wordToAdd) {
	return {
		type: 'ADDWORD',
		wordToAdd,
	}
}

export function setCurrentWord(wordToSet) {
	return {
		type: 'SETCURRENTWORD',
		wordToSet,
	}
}
