import React from 'react';
import { StyleSheet, View, TextInput, Text, Button } from 'react-native';
import ComponentA from './components/ComponentA.js';
import ComponentB from './components/ComponentB.js';
import { Provider } from 'react-redux';
import {useSelector, useDispatch } from 'react-redux';
import { addWord, setCurrentWord } from './redux/action.js';
import store from './redux/store';


function ComponentC () {
	const applicationState = useSelector(state => state);
	const dispatch = useDispatch();

	return (
		<View style={styles.container}>
			<Text>{`${applicationState.words}`}</Text>
			<Text>{`Current word here ${applicationState.currentWord}`}</Text>
			<TextInput onChangeText={value => dispatch(setCurrentWord(value))} placeholder={`Enter word to add to redux managed array`}/>
			<Button title={`Add Word To Redux Managed Array`} onPress={
				function(){

					// update redux managed array
					dispatch(addWord(applicationState.currentWord));
					// clear redux managed string
					dispatch(setCurrentWord(''));
					console.log('stub');
				}} />
		</View>
	)
}

export default function App() {
	return (
		<View style={styles.container}>
			<Provider store={store}>
				<ComponentC/>
				<View style={styles.container}>
					<ComponentA/>
					<ComponentB/>
				</View>
			</Provider>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
});
